# -*- coding: utf-8 -*-
import scrapy


class Level1techsSpider(scrapy.Spider):
    name = 'level1techs'
    allowed_domains = ['level1techs.com/podcasts/feed']
    start_urls = ['http://level1techs.com/podcasts/feed/']

    def parse(self, response):
        titles = response.xpath('//item/title/text()').extract()
        dates = response.xpath('//item/pubDate/text()').extract()
        descriptions = response.xpath('//item/description/text()').extract()
        podcasts = response.xpath('//item/enclosure/@url').extract()

        for item in zip(titles,dates,descriptions,podcasts):
            scraped_info = {
                    'title' : item[0],
                    'date' : item[1],
                    'description' : item[2],
                    'podcast' : item[3]
            }

            yield scraped_info
